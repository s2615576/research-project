use std::fs::File;
use std::io::Write;
use std::time::{Instant, Duration};
use std::{thread, println};
use petgraph::graph::{NodeIndex, UnGraph};
use petgraph::algo::{dijkstra};
use petgraph::dot::{Dot, Config};
use std::sync::{Arc, Mutex};

//part of the algorithms for generating trees
fn next_tree(candidate: &Vec<usize>) -> Option<Vec<usize>> {
    let (left, rest): (Vec<usize>, Vec<usize>,) = split_tree(&candidate);

    // b) condition
    let left_height: &usize = left.iter().max().unwrap();
    let rest_height: &usize = rest.iter().max().unwrap();
    let mut valid: bool = rest_height >= left_height;

    // these are c) and d) condition
    if valid && rest_height == left_height {
        if (left.len() > rest.len()) || (left.len() == rest.len() && left > rest) {
            valid = false;
        }
    }

    if valid {
        return Some(candidate.to_vec());
    }
    else{
        let p: usize = left.len();
        let mut new_candidate: Vec<usize>;
        match next_rooted_tree(candidate, p, false){
            Some(c) => new_candidate = c,
            None    => return None,
        }

        if candidate[p] > 2{
            let (new_left, _): (Vec<usize>, _) = split_tree(&new_candidate);
            let new_left_height: &usize = new_left.iter().max().unwrap();
            let suffix: std::ops::Range<usize> = 1 .. new_left_height + 2;
            let length: usize = new_candidate.len();

            new_candidate.splice(length - new_left_height - 1 .. length , suffix);
        }
        return Some(new_candidate);
    }
}

//part of the algorithms for generating trees
fn next_rooted_tree(predecessor: &Vec<usize>, p: usize, calc_p: bool) -> Option<Vec<usize>> {
    let mut p: usize = p;
    if calc_p {
        p = predecessor.len() - 1;
        while predecessor[p] == 1{
            p -= 1;
        }
    }
    if p == 0 {
        return None;
    }

    let mut q: usize = p - 1;
    while predecessor[q] != predecessor[p] - 1{
        q -= 1;
    }
    let mut result: Vec<usize> = predecessor.clone();
    for i in p .. result.len() {
        result[i] = result [i - p + q]
    }
    return Some(result);
}

//part of the algorithms for generating trees
fn split_tree(layout: &Vec<usize>)-> (Vec<usize>, Vec<usize>){
    let mut one_found: bool = false;
    let mut m: usize = layout.len();
    for i in 0 .. layout.len(){
        if layout[i] == 1{
            if one_found{
                // m = Some(i);
                m = i;
                break;
            }
            else{
                one_found = true;
            }
        }
    }
    let mut left: Vec<usize> = Vec::with_capacity(m - 1);
    let mut rest: Vec<usize> = Vec::with_capacity(layout.len() - m + 1);
    for i in 1 .. m {
        left.push(layout[i] - 1);
    }
    rest.push(0);
    for i in m .. layout.len() {
        rest.push(layout[i]);
    }
    return (left, rest);
}

//convert layout(integer sequence) to a graph object
fn layout_to_graph(layout: &Vec<usize>) -> UnGraph<usize, i8> {
    let mut graph = UnGraph::<usize, i8>::new_undirected();
    // add nodes
    for i in 0 .. layout.len(){
        graph.add_node(i);
    } 
    let mut stack = Vec::new();
    for i in 0 .. layout.len(){
        let i_level = layout[i];
        if !stack.is_empty(){
            let mut j = stack[stack.len() - 1];
            let mut j_level = layout[j];
            while j_level >= i_level{
                stack.pop();
                j = stack[stack.len() - 1];
                j_level = layout[j];
            }
            graph.add_edge(NodeIndex::new(i), NodeIndex::new(j), 1);
        }
        stack.push(i);
    }
    return graph;
}

// calculate wiener entropy of a graph
fn calculate_wiener_entropy(graph: &UnGraph<usize, i8>) -> f64{
    //get all transitions
    let transmissions: Vec<usize> = calculate_all_transmissions(&graph);

    //calculate wiener index
    let wiener_index: f64 = transmissions.clone().into_iter().sum::<usize>() as f64;

    //calculate entropy
    let entropy: f64 = wiener_index.log2() - ((1.0 / wiener_index) * transmissions.into_iter().map(|t| t as f64).map(|t| t * t.log2()).sum::<f64>()); // fix this shit
    
    return entropy;
}

// calculate all transmissions of a graph
fn calculate_all_transmissions(graph: &UnGraph<usize, i8>) -> Vec<usize> {
    let mut transmissions:Vec<usize> = Vec::new();

    // go through every node in the graph and run dijkstra algorithm to calculate the all the shortest paths
    // then sum all the distances
    for u in graph.node_indices(){
        transmissions.push(dijkstra(&graph, u, None, |_| 1).values().sum());
    }

    return transmissions;
}

//calculate the top nth min graphs entropies and graphs
fn nth_min_entropy_graphs(order: usize, nth: usize) -> Vec<(f64, Vec<UnGraph<usize, i8>>)> {
    let mut init_layout: Vec<usize> = (0 .. (order / 2 + 1)).collect::<Vec<usize>>();
    init_layout.extend((1 .. (order + 1) / 2).collect::<Vec<usize>>());

    let layout: Option<Vec<usize>> = Some(init_layout);

    let min_e_g = Arc::new(Mutex::new(vec![(f64::MAX, Vec::new()); nth]));

    let n_workers: usize = 5;   // number of consumers
    let n_producers: usize = 1; // number of producers

    let r = {
        let (s, r) = chan::sync(n_workers * 2);
        let (slayout, rlayout) = chan::sync::<Option<Vec<usize>>>(n_producers);
        slayout.send(layout);
        // produce
        for _ in 0..n_producers{
            let s = s.clone();
            let slayout = slayout.clone();
            let rlayout = rlayout.clone();
            thread::spawn(move || {
                //loop through the layouts in the channel
                for layout in rlayout{
                    if layout.is_none() {
                        slayout.send(None);  // send signal for the other producers to stop too
                        break;
                    }
                    let inner_layout: Vec<usize> = layout.unwrap();
                    // produce a tree
                    let layout: Option<Vec<usize>> = next_tree(&inner_layout);
                    if layout.is_some(){
                        let inner_layout = layout.unwrap();
                        
                        // produce next rooted tree and put it in the producers channel so another thread can already start
                        slayout.send(next_rooted_tree(&inner_layout, 0, true));
                        
                        // send the produced tree
                        s.send(inner_layout);
                    }
                }
            });
        }
        r
    };

    //consume
    let wg = chan::WaitGroup::new();
    for _ in 0..n_workers {
        wg.add(1);
        let wg = wg.clone();
        let r = r.clone();
        let min_e_g = Arc::clone(&min_e_g);
        thread::spawn(move || {
            // loop through produced trees
            for layout in r {
                let graph = layout_to_graph(&layout); // make a graph from the integer sequence
                let entropy: f64 = calculate_wiener_entropy(&graph); // calculate entropy

                let mut min_e_g = min_e_g.lock().unwrap();
                // check if the entropy is in the top nth
                for i in 0 .. min_e_g.len() {
                    if entropy < min_e_g[i].0 {
                        min_e_g.insert(i, (entropy, vec![graph]));
                        min_e_g.pop();
                        break;
                    } else if entropy == min_e_g[i].0 {
                        min_e_g[i].1.push(graph);
                        break;
                    }
                }           
            }
            wg.done();
        });
    }
    wg.wait();

    let min_e_g: Vec<(f64, Vec<petgraph::Graph<usize, i8, petgraph::Undirected>>)> = min_e_g.lock().unwrap().to_vec();

    return min_e_g;
}


pub fn main() {
    let order: usize = 21; // size of the tree to be calculated
    let nth: usize = 4;    // how many of the minimum graphs to be saved

    let start: Instant = Instant::now();
    let min_e_g = nth_min_entropy_graphs(order, nth); // calculate minimum graphs
    let duration: Duration = start.elapsed();

    // go through top n minimum graphs
    for (nth, (entropy, graphs)) in min_e_g.into_iter().enumerate() {
        println!("nth {} Minimal entropy for N = {} is {} ({:?})", nth + 1, order, entropy, duration);
        // save the graphs in txt files in dot format
        for (i, graph) in graphs.iter().enumerate(){
            // if there are more than 1 graphs use the index to change the name of the file
            if graphs.len() == 1 {
                let mut file = File::create(format!("{} tree {} - {}.txt",nth, order, entropy)).unwrap();
                let output = Dot::with_config(&graph, &[Config::EdgeNoLabel]).to_string();
                let _ = file.write_all(&output.as_bytes());
            }
            else{
                let mut file = File::create(format!("{} tree {} - {} ({}).txt",nth, order, entropy, i)).unwrap();
                let output = Dot::with_config(&graph, &[Config::EdgeNoLabel]).to_string();
                let _ = file.write_all(&output.as_bytes());
            }
        }
    }
}
