use csv::Writer;
use std::time::{Instant};
use std::{println, thread};
use std::sync::{Arc, Mutex};

// return the minimum entropy and the set of minimal graphs, it is a vec in case there is more than 1
fn min_broom(n: usize) -> (f64, Vec<usize>){
    let mut min_ent_graph: (f64, Vec<usize>) = (f64::MAX, Vec::new());

    // go through all possible k
    for k in 1 .. n {
        let entropy = calculate_entropy(n, k);

        if entropy < min_ent_graph.0 {
            min_ent_graph = (entropy, vec![k]);
        }
        else if entropy == min_ent_graph.0 {
            min_ent_graph.1.push(k);
        }
    }

    return min_ent_graph;
}

// calculate entropy of a broom with given size(n) and length of path(k)
fn calculate_entropy(n: usize, k: usize) -> f64 {

    // pendant nodes n - k
    let transmission_a = (k * (k + 1) / 2 + (n - k - 1) * 2) as f64;
    let mut transmissions_b = 0 as f64; // sum of the transmissions of the nodes on the path
    let mut entropy_b = 0 as f64; // intermediate result of sum(transmission * log(transmission)) for the nodes on the path

    // go through the nodes on the path and calculate transmissions_b and entropy_b
    for i in 1 .. k + 1 {
        let transmission = ((k - i)*(k - i + 1)/2 + i * (i - 1) / 2 + (n - k) * i) as f64;
        entropy_b += transmission * transmission.log2();
        transmissions_b += transmission;
    }

    let wiener_index = (n - k) as f64 * transmission_a + transmissions_b;

    let entropy = wiener_index.log2() - (1.0 / wiener_index) * ((n - k) as f64 * transmission_a * transmission_a.log2() + entropy_b);

    return entropy;
}


pub fn main() {
    let wtr = Arc::new(Mutex::new(Writer::from_path("min_brooms.csv").unwrap()));
    let start_n = 18;
    let end_n = 125_000;
    let n_core = 4;
    let start: Instant = Instant::now();
    let wg = chan::WaitGroup::new(); // barrier to wait all threads
    // spawn threads
    for i in 0 .. n_core {
        wg.add(1);
        let wg = wg.clone();
        let wtr = Arc::clone(&wtr);

        thread::spawn(move || {
            let increment = n_core;
            for n in (start_n + i .. end_n).step_by(increment){
                // calculate minimal broom for n
                let (entropy, ks) = min_broom(n);

                // append the result to a csv file
                let mut wtr = wtr.lock().unwrap();
                for k in &ks {
                    let _ = wtr.write_record(&[n.to_string(), k.to_string(), entropy.to_string()]);
                    let _ = wtr.flush();
                }
            }
            // signal that the thread is done
            wg.done();
        });
    }
    // wait for all the threads
    wg.wait();
    // print the time it took
    println!("time: {:?}", start.elapsed());
}
