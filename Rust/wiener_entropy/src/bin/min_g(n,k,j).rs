use std::thread;
use std::sync::{Arc, Mutex};
use csv::Writer;
use decimal::d128;

// calculate the entropy of G(n, k, j) graph using 64 bits float
fn calculate_entropy_f64(n: usize, k: usize, j: usize) -> f64 {
    // transmission of a node in the clique which is not connected to the path
    let transmission_a: f64 = ((n - k - 1) + (k * (k + 1))/2 + k) as f64;

    // transmission of a node in the clique which is connected to the path
    let transmission_b: f64 = ((n - k - 1) +  (k * (k + 1))/2) as f64;

    // sum of the transmission * log(transmission) for each node on the path
    let mut entropy_c: f64 = 0.0;
    // sum of the transmissions of the nodes in the path
    let mut transmissions_c: f64 = 0.0;
    for i in 1 .. k + 1 {
        let transmission: f64 = ((k - i)*(k - i + 1)/2 + i * (i - 1) / 2 + j * i + (n - k - j)*(i + 1)) as f64;
        entropy_c += transmission * transmission.log2(); 
        transmissions_c += transmission;
    }

    let number_of_a = (n - k - j) as f64;
    let number_of_b = j as f64;

    let wiener_index: f64 = number_of_a * transmission_a + number_of_b * transmission_b + transmissions_c;
    let entropy = wiener_index.log2() - ((1.0 / wiener_index) * (number_of_a * transmission_a * transmission_a.log2() + number_of_b * transmission_b * transmission_b.log2() + entropy_c));
    return entropy;
}

// calculate the minimum entropy of G(n, k, j) graphs with given size(n) using 64 bit floats
fn min_gnkj_f64(n: usize) -> (f64, Vec<(usize, usize)>) {
    let min_entropy_k_j = Arc::new(Mutex::new((f64::MAX, Vec::new()))); // here the results will be saved

    let wg = chan::WaitGroup::new();
    // go through each k possible
    for k in 1 .. n {
        wg.add(1);
        let wg = wg.clone();
        let min_entropy_k_j = Arc::clone(&min_entropy_k_j);
        // go thorugh each j possible and let a sepparate thread to calculate that
        thread::spawn(move || {
            for j in 1 .. n - k + 1 {
                let entropy = calculate_entropy_f64(n, k, j);

                let mut min_entropy_k_j = min_entropy_k_j.lock().unwrap();
                if entropy < min_entropy_k_j.0 {
                    min_entropy_k_j.0 = entropy;
                    min_entropy_k_j.1 = Vec::new();
                    min_entropy_k_j.1.push((k, j));
                }
                else if entropy == min_entropy_k_j.0 {
                    min_entropy_k_j.1.push((k, j));
                }
            }
            wg.done(); // signal that the thread is done
        });
    }
    wg.wait(); // wait for all threads to finish
    let min_entropy_k_j = min_entropy_k_j.lock().unwrap();
    let (min_entropy, k_j):(f64, Vec<(usize, usize)>) = (min_entropy_k_j.0, min_entropy_k_j.1.to_vec());

    return (min_entropy, k_j);
}
// calculate the entropy of G(n, k, j) graph using 64 bits float
fn calculate_entropy_d128(n: usize, k: usize, j: usize) -> d128 {
    // transmission of a node in the clique which is not connected to the path
    let transmission_a: d128 = d128::from(((n - k - 1) + (k * (k + 1))/2 + k) as i32);

    // transmission of a node in the clique which is connected to the path
    let transmission_b: d128 = d128::from(((n - k - 1) +  (k * (k + 1))/2) as i32);

    // sum of the transmission * log(transmission) for each node on the path
    let mut entropy_c: d128 = d128!(0.0);
    // sum of the transmissions of the nodes in the path
    let mut transmissions_c: d128 = d128!(0.0);
    for i in 1 .. k + 1 {
        let transmission: d128 = d128::from(((k - i)*(k - i + 1)/2 + i * (i - 1) / 2 + j * i + (n - k - j)*(i + 1)) as i32);
        entropy_c += transmission * transmission.ln() / d128!(2).ln(); 
        transmissions_c += transmission;
    }
    let number_of_a = d128::from((n - k - j) as i32);
    let number_of_b = d128::from(j as i32);

    let wiener_index: d128 = number_of_a * transmission_a + number_of_b * transmission_b + transmissions_c;
    let entropy = wiener_index.ln() / d128!(2).ln() - ((d128!(1.0) / wiener_index) * (number_of_a * transmission_a * transmission_a.ln() / d128!(2).ln() + number_of_b * transmission_b * transmission_b.ln() / d128!(2).ln() + entropy_c));

    return entropy;
}

// calculate the minimum entropy of G(n, k, j) graphs with given size(n) using 64 bit floats
fn min_gnkj_d128(n : usize) -> (d128, Vec<(usize, usize)>) {
    let min_entropy_k_j = Arc::new(Mutex::new((d128::infinity(), Vec::new())));

    let wg = chan::WaitGroup::new();
    for k in 1 .. n {
        wg.add(1);
        let wg = wg.clone();
        let min_entropy_k_j = Arc::clone(&min_entropy_k_j);
        thread::spawn(move || {
            for j in 1 .. n - k + 1 {
                let entropy = calculate_entropy_d128(n, k, j);

                let mut min_entropy_k_j = min_entropy_k_j.lock().unwrap();
                if entropy < min_entropy_k_j.0 {
                    min_entropy_k_j.0 = entropy;
                    min_entropy_k_j.1 = Vec::new();
                    min_entropy_k_j.1.push((k, j));
                }
                else if entropy == min_entropy_k_j.0 {
                    min_entropy_k_j.1.push((k, j));
                }
            }
            wg.done();
        });
    }
    wg.wait();
    let min_entropy_k_j = min_entropy_k_j.lock().unwrap();
    let (min_entropy, k_j):(d128, Vec<(usize, usize)>) = (min_entropy_k_j.0, min_entropy_k_j.1.to_vec());

    return (min_entropy, k_j);
}
    
pub fn main() {
    // create the csv
    let mut wtr = Writer::from_path("min_g(n,k,j).csv").unwrap();
    let _ = wtr.write_record(&["n", "k", "j", "entropy"]);
    for n in 16 .. 10_000 {
        let (entropy, k_j) = min_gnkj_f64(n); // calculate minimal graph
        // append results to the csv
        for (k, j) in k_j{
            let _ = wtr.write_record(&[n.to_string(), k.to_string(), j.to_string(), entropy.to_string()]);
        }
        let _ = wtr.flush();
    }
}
