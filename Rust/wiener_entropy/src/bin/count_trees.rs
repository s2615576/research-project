use std::time::{Instant};
use std::{println};

//part of the algorithms for generating trees
fn next_tree(candidate: &Vec<usize>) -> Option<Vec<usize>> {
    let (left, rest): (Vec<usize>, Vec<usize>,) = split_tree(&candidate);

    // b) condition
    let left_height: &usize = left.iter().max().unwrap();
    let rest_height: &usize = rest.iter().max().unwrap();
    let mut valid: bool = rest_height >= left_height;

    // these are c) and d) condition
    if valid && rest_height == left_height {
        if (left.len() > rest.len()) || (left.len() == rest.len() && left > rest) {
            valid = false;
        }
    }

    if valid {
        return Some(candidate.to_vec());
    }
    else{
        let p: usize = left.len();
        let mut new_candidate: Vec<usize>;
        match next_rooted_tree(candidate, p, false){
            Some(c) => new_candidate = c,
            None    => return None,
        }

        if candidate[p] > 2{
            let (new_left, _): (Vec<usize>, _) = split_tree(&new_candidate);
            let new_left_height: &usize = new_left.iter().max().unwrap();
            let suffix: std::ops::Range<usize> = 1 .. new_left_height + 2;
            let length: usize = new_candidate.len();

            new_candidate.splice(length - new_left_height - 1 .. length , suffix);
        }
        return Some(new_candidate);
    }
}

//part of the algorithms for generating trees
fn next_rooted_tree(predecessor: &Vec<usize>, p: usize, calc_p: bool) -> Option<Vec<usize>> {
    let mut p: usize = p;
    if calc_p {
        p = predecessor.len() - 1;
        while predecessor[p] == 1{
            p -= 1;
        }
    }
    if p == 0 {
        return None;
    }

    let mut q: usize = p - 1;
    while predecessor[q] != predecessor[p] - 1{
        q -= 1;
    }
    let mut result: Vec<usize> = predecessor.clone();
    for i in p .. result.len() {
        result[i] = result [i - p + q]
    }
    return Some(result);
}

//part of the algorithms for generating trees
fn split_tree(layout: &Vec<usize>)-> (Vec<usize>, Vec<usize>){
    let mut one_found: bool = false;
    let mut m: usize = layout.len();
    for i in 0 .. layout.len(){
        if layout[i] == 1{
            if one_found{
                // m = Some(i);
                m = i;
                break;
            }
            else{
                one_found = true;
            }
        }
    }
    let mut left: Vec<usize> = Vec::with_capacity(m - 1);
    let mut rest: Vec<usize> = Vec::with_capacity(layout.len() - m + 1);
    for i in 1 .. m {
        left.push(layout[i] - 1);
    }
    rest.push(0);
    for i in m .. layout.len() {
        rest.push(layout[i]);
    }
    return (left, rest);
}


pub fn main() {
    let n: usize = 25; // size of trees
    let start: Instant = Instant::now();

    let mut init_layout: Vec<usize> = (0 .. (n / 2 + 1)).collect::<Vec<usize>>();
    init_layout.extend((1 .. (n + 1) / 2).collect::<Vec<usize>>());

    let mut layout = Some(init_layout);
    let mut count: u64 = 0;
    loop{
        if layout.is_none() {
            break;
        }
        let inner_layout: Vec<usize> = layout.unwrap();

        // produce a tree
        layout = next_tree(&inner_layout);

        if layout.is_some() {
            let inner_layout = layout.unwrap();
            count += 1;
            layout = next_rooted_tree(&inner_layout, 0, true);
        }
    }

    println!("N: {n}, Count: {count} ({:?})", start.elapsed());

}
