import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


def plot_regression_line_brooms(loglog=False):
    print("Broom")
    df = pd.read_csv('min_brooms.csv', header=0)
    
    # observations / data
    n = df['n'].to_numpy()
    k = df['k'].to_numpy()
    entropy = df['entropy'].to_numpy()
    
    ##############################################################
    # plotting the actual points as scatter plot
    fig = plt.figure("broom k")    
    ax = plt.gca()
    if loglog:
        ax.set_yscale('log')
        ax.set_xscale('log')
    ax.scatter(n, k, color = "g", marker = ".")
    
    # calculate a and b and plot the regression line
    fit = np.polyfit(np.log2(n), np.log2(k), 1)
    a = fit[0]
    b = fit[1]
    epsilon = a - 0.5
    print(f'{a=}, {b=}, {epsilon=}')
    
    y_pred = np.exp2(b) * np.float_power(n, a)
    ax.plot(n, y_pred, color = 'r')
    
    # putting labels and legend
    ax.set_xlabel('n')
    ax.set_ylabel('k')
    plt.legend(["actual k", "log regressin k"], loc ="lower right")
    ##############################################################
    fig = plt.figure("broom entropy")
    ax = plt.gca()
    
    if loglog:
        ax.set_yscale('log')
        ax.set_xscale('log')
    ax.scatter(n, entropy, color = "g", marker = ".")
    
    # #predicted entropy using epsilon
    entropy_pred = (3 + 2*epsilon)/4 * np.log2(n)
    lower_bound = (3 + 0)/4 * np.log2(n)
    upper_bound = (3 + 2/3)/4 * np.log2(n)
    ax.plot(n, entropy_pred, color = 'y')
    ax.plot(n, lower_bound, color = 'b')
    ax.plot(n, upper_bound, color = 'r')
    
    ax.set_xlabel('n')
    ax.set_ylabel('entropy')
    plt.legend(["actual entropy", "predicted entropy", "lower bound", "upper bound"], loc ="lower right")
    ##############################################################
    plt.show()
    
def plot_regression_line_gnkj(loglog=False):
    print("G(n, k, j)")
    df = pd.read_csv('min_g(n,k,j).csv', header=0)
    
    # observations / data
    n = df['n'].to_numpy()
    k = df['k'].to_numpy()
    j = df['j'].to_numpy()
    entropy = df['entropy'].to_numpy()
    
    ##############################################################
    # plotting the actual points as scatter plot
    fig = plt.figure("G(n, k, j) k relation")
    ax = plt.gca()
    ax.scatter(n, k, color = "g", marker = ".")
    
    if loglog:
        ax.set_yscale('log')
        ax.set_xscale('log')
    
    # calculate a and b and plot the regression line
    fit = np.polyfit(np.log2(n), np.log2(k), 1)
    a = fit[0]
    b = fit[1]
    print(f'{a=} {b=}')
    y_pred = np.exp2(b) * np.float_power(n, a)
    ax.plot(n, y_pred, color = 'r')
    
    # putting labels
    ax.set_xlabel('n')
    ax.set_ylabel('k')
    plt.legend(["actual k", "log regression of k"], loc ="lower right")
    ##############################################################

    # plotting the actual points as scatter plot
    fig = plt.figure("G(n, k, j) entropy")    
    ax = plt.gca()
    if loglog:
        ax.set_yscale('log')
        ax.set_xscale('log')
    ax.scatter(n, entropy, color = "g", marker = ".")
    
    # calculate a and b and plot the regression line
    fit = np.polyfit(np.log2(n), np.log2(entropy), 1)
    a = fit[0]
    b = fit[1]
    print(f'{a=} {b=}')
    y_pred = np.exp2(b) * np.float_power(n, a)
    ax.plot(n, y_pred, color = 'r')
    
    # putting labels
    ax.set_xlabel('n')
    ax.set_ylabel('entropy')
    plt.legend(["actual entropy", "log regression"], loc ="lower right")
    ##############################################################
    plt.show()
    

def plot_j_gnknj(loglog=False):
    df = pd.read_csv('min_g(n,k,j).csv', header=0)
    # observations / data
    n = df['n'].to_numpy()
    k = df['k'].to_numpy()
    j = df['j'].to_numpy()
    filtered = df.groupby('n').filter(lambda x: len(x) > 1)
    filt_n = np.unique(filtered['n'].to_numpy())
    # filt_k = filtered['k'].to_numpy()
    filt_j = filtered['j'].to_numpy()
    filt_j = filt_j[np.where(filt_j != 1)]
    
    ##############################################################
    fig = plt.figure("G(n, k, j) graph j to n relation")
    ax = plt.gca()
    if loglog:
        ax.set_yscale('log')
        ax.set_xscale('log')
    # actual data
    ax.scatter(n, j, color = "g", marker = ".")
    
    # n - k line
    ax.plot(n, n - k, color='b')
    
    ax.set_xlabel('n')
    ax.set_ylabel('j')
    plt.legend(['actual data', 'n - k']) 
    plt.show()


def plot_j_buckets():
    df = pd.read_csv('min_g(n,k,j).csv', header=0)
    
    filtered_1min = df.groupby('n').filter(lambda x: len(x) == 1)
    filtered_2min = df.groupby('n').filter(lambda x: len(x) > 1)
    n_1min = filtered_1min['n'].to_numpy()
    n_2min = np.unique(filtered_2min['n'].to_numpy())
    
    fig = plt.figure("G(n, k, j) 1 min and 2 min percantage")
    ax = plt.gca()
    ax.hist(n_1min, 20, weights = np.ones(len(n_1min)) * 100/250, edgecolor = "black")    
    print(len(n_1min))
    
    # ax.hist(n_1min, 103, color=None)
    ax.set_xlabel('n in intervals of 250')
    ax.set_ylabel('%')
    
    plt.legend(['% of graphs with 1 minimum'])
    ##############################################################
    
    fig = plt.figure("G(n, k, j) j value for 1 minimum graphs")
    ax = plt.gca()
    n_j_1 = filtered_1min[filtered_1min['j'] == 1]['n'].to_numpy()
    n_j_other = filtered_1min[filtered_1min['j'] != 1]['n'].to_numpy()

    ax.hist([n_j_1, n_j_other], 20) 
    ax.set_xlabel('n in intervals of 250')
    ax.set_ylabel('count')
    plt.legend(['minimum has j = 1', 'minimum has j != 1']) 
    
    ##############################################################
    fig = plt.figure("G(n, k, j) barstacked")
    ax = plt.gca()
    ax.hist([n_2min, n_j_1, n_j_other], 20, edgecolor = "black", histtype='barstacked')    
    ax.set_xlabel('n in intervals of 250')
    ax.set_ylabel('# of graphs')
    plt.legend(['2 minimums', '1 minimums with j = 1', '1 minimums with j != 1'], loc = "lower right") 
    plt.show()


def count_j():
    df = pd.read_csv('min_g(n,k,j).csv', header=0)

    filtered_2min = df.groupby('n').filter(lambda x: len(x) > 1)
    count_max_j = 0
    sum_k_1 = 0
    sum_k_not1 = 0
    for index, row in filtered_2min.iterrows():
        if row['j'] == 1:
            sum_k_1 += row['k']
        else:
            sum_k_not1 += row['k']
            
        if row['n'] - row['k'] == row['j']:
            count_max_j += 1
    print(f'{count_max_j=}, out of {filtered_2min.shape[0] / 2}')
    print(f'{sum_k_1=}, {sum_k_not1=}, difference: {sum_k_not1 - sum_k_1}, number of 2 min graphs: {filtered_2min.shape[0] / 2}') # if second minimum is always with k + 1 the difference should be the number of 2 min graphs

if __name__ == "__main__": 
    # plotting regression line for brooms
    plot_regression_line_brooms(loglog=True)
    # plot_regression_line_brooms(loglog=False)
 
    # plotting regression line for G(n, k, j)
    plot_regression_line_gnkj(loglog=True)
    # plot_regression_line_gnkj(loglog=False)
    
    plot_j_gnknj(loglog=False)
    
    plot_j_buckets()
    
    count_j()